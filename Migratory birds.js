'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the migratoryBirds function below.
function migratoryBirds(arr) {
    
    var countType1 = 0;
    var countType2 = 0;
    var countType3 = 0;
    var countType4 = 0;
    var countType5 = 0;

    for (var i = 0; i < arr.length; i++)
    {
        switch (arr[i])
        {
            case 1: countType1 = countType1 + 1;
                break;
            case 2: countType2 = countType2 + 1;
                break;
            case 3: countType3 = countType3 + 1;
                break;
            case 4: countType4 = countType4 + 1;
                break;
            case 5: countType5 = countType5 + 1;
                break;
            default: break;
        }
    }
    let arrTemp = [countType1, countType2, countType3, countType4, countType5];
    var max = arrTemp[0];
    for (var j = 0; j <arrTemp.length; j++)
    {
        if (arrTemp[j] > max)
            max = arrTemp[j];
    }
    for (var k = 0; k < arrTemp.length; k++)
    {
        if (max == arrTemp[k])
        {
            return k+1;
        }
    }
}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const arrCount = parseInt(readLine().trim(), 10);

    const arr = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));

    const result = migratoryBirds(arr);

    ws.write(result + '\n');

    ws.end();
}
